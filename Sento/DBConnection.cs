﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Sento
{
    class DBConnection: SentoDBDataContext
    {
        private static DBConnection instance;
        private static readonly object padlock = new object();
        private static string connectionString = global::Sento.Properties.Settings.Default.SentoDBConnectionString;
        private static SqlConnection connect;
        private SqlDataAdapter adapter;
        public  SqlCommand cmd;
        private static SqlCommandBuilder cmdBuilder;
        private static DataSet dataSet = new DataSet();

        private DBConnection() {
            connect = new SqlConnection(connectionString);
        }

        public static DBConnection Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new DBConnection();
                    }
                    return instance;
                }
            }
        }
        public void RunProcedure(string procedureName)
        {
            cmd = new SqlCommand(procedureName, connect);
            cmd.CommandType = CommandType.StoredProcedure;
        }
        public SqlCommand getCommand()
        {
            return cmd;
        }
        public void AddParameters(SqlParameter parameter)
        {
            cmd.Parameters.Add(parameter);
        }
        public void LoadProcedure()
        {
            connect.Open();
            cmd.ExecuteNonQuery();
            connect.Close();
        }
        public DataTable runScript(string script)
        {
            DataTable table = new DataTable();
            adapter = new SqlDataAdapter(script, connect);
            adapter.Fill(table);
            return table;
        }
        public void OpenConnection()
        {
            connect.Open();
        }
        public void CloseConnection()
        {
            connect.Close();
        }
    }
}
