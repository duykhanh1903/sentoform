﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sento
{
    public class Converter
    {
        public static string GetDetailString(DataGridViewRow row)
        {
            List<string> result= new List<string>();
            for (int i = 0; i < row.Cells.Count; i++)
            {
                string header = row.DataGridView.Columns[i].HeaderText;
                object value = row.Cells[i].Value;
                string cell = (value ?? string.Empty).ToString();
                string detail = header + ": " + cell;
                if(header != "")
                    result.Add(detail);
            }
            return string.Join("\n", result);
        }
    }
}
