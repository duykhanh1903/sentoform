﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sento
{
    public partial class permission_UC : UserControl
    {
        public permission_UC()
        {
            InitializeComponent();

            treeView1.Nodes.Add("Quản Trị");
            treeView1.Nodes.Add("Ban Quản Lí");
            treeView1.Nodes.Add("Nhân Viên");

            treeView1.Nodes[0].Nodes.Add("Chủ Tịch");
            treeView1.Nodes[0].Nodes.Add("Phó Chủ Tịch");

            treeView1.Nodes[1].Nodes.Add("Tổng Giám Đốc");
            treeView1.Nodes[1].Nodes.Add("Phó Giám Đốc");
            treeView1.Nodes[1].Nodes.Add("Giám đóc Kĩ Thuật");
            treeView1.Nodes[1].Nodes.Add("Trưởng Phòng Kinh Doanh");
            treeView1.Nodes[1].Nodes.Add("Trưởng Phòng Nhân Sự");
            treeView1.Nodes[1].Nodes.Add("Trưởng Phòng Vật Tư");

            treeView1.Nodes[2].Nodes.Add("Nhân Viên Phòng Kinh Doanh");
            treeView1.Nodes[2].Nodes.Add("Nhân Viên Phòng Kĩ Thuật");
            treeView1.Nodes[2].Nodes.Add("Nhân Viên Công Đoàn");
            treeView1.Nodes[2].Nodes.Add("Nhân Viên phòng IT");
        }
    }
}
