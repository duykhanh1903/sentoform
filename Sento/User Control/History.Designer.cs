﻿namespace Sento
{
    partial class History
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.filterTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.detailGroupBox = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.submitButton = new System.Windows.Forms.Button();
            this.fieldTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.overallTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.historyGroupBox = new System.Windows.Forms.GroupBox();
            this.detailRichTextBox = new System.Windows.Forms.RichTextBox();
            this.historyDataGridView = new System.Windows.Forms.DataGridView();
            this.index = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.function = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.user = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.filterTableLayoutPanel.SuspendLayout();
            this.detailGroupBox.SuspendLayout();
            this.panel2.SuspendLayout();
            this.fieldTableLayoutPanel.SuspendLayout();
            this.overallTableLayoutPanel.SuspendLayout();
            this.panel3.SuspendLayout();
            this.historyGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.historyDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // filterTableLayoutPanel
            // 
            this.filterTableLayoutPanel.BackColor = System.Drawing.SystemColors.Control;
            this.filterTableLayoutPanel.ColumnCount = 2;
            this.filterTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.filterTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.filterTableLayoutPanel.Controls.Add(this.detailGroupBox, 0, 0);
            this.filterTableLayoutPanel.Controls.Add(this.panel2, 1, 0);
            this.filterTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filterTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.filterTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.filterTableLayoutPanel.Name = "filterTableLayoutPanel";
            this.filterTableLayoutPanel.RowCount = 1;
            this.filterTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.filterTableLayoutPanel.Size = new System.Drawing.Size(1248, 303);
            this.filterTableLayoutPanel.TabIndex = 0;
            // 
            // detailGroupBox
            // 
            this.detailGroupBox.Controls.Add(this.detailRichTextBox);
            this.detailGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detailGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.detailGroupBox.Location = new System.Drawing.Point(3, 3);
            this.detailGroupBox.Name = "detailGroupBox";
            this.detailGroupBox.Size = new System.Drawing.Size(618, 297);
            this.detailGroupBox.TabIndex = 2;
            this.detailGroupBox.TabStop = false;
            this.detailGroupBox.Text = "Chi Tiết";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.submitButton);
            this.panel2.Controls.Add(this.fieldTableLayoutPanel);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(627, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(618, 297);
            this.panel2.TabIndex = 3;
            // 
            // submitButton
            // 
            this.submitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.submitButton.BackColor = System.Drawing.SystemColors.Highlight;
            this.submitButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.submitButton.Location = new System.Drawing.Point(515, 252);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(90, 39);
            this.submitButton.TabIndex = 5;
            this.submitButton.Text = "Tìm";
            this.submitButton.UseVisualStyleBackColor = false;
            // 
            // fieldTableLayoutPanel
            // 
            this.fieldTableLayoutPanel.ColumnCount = 2;
            this.fieldTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.fieldTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.fieldTableLayoutPanel.Controls.Add(this.comboBox2, 1, 4);
            this.fieldTableLayoutPanel.Controls.Add(this.label5, 0, 4);
            this.fieldTableLayoutPanel.Controls.Add(this.comboBox1, 1, 3);
            this.fieldTableLayoutPanel.Controls.Add(this.label4, 0, 3);
            this.fieldTableLayoutPanel.Controls.Add(this.label3, 0, 2);
            this.fieldTableLayoutPanel.Controls.Add(this.textBox3, 1, 2);
            this.fieldTableLayoutPanel.Controls.Add(this.label2, 0, 1);
            this.fieldTableLayoutPanel.Controls.Add(this.textBox2, 1, 1);
            this.fieldTableLayoutPanel.Controls.Add(this.label1, 0, 0);
            this.fieldTableLayoutPanel.Controls.Add(this.textBox1, 1, 0);
            this.fieldTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fieldTableLayoutPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fieldTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.fieldTableLayoutPanel.Name = "fieldTableLayoutPanel";
            this.fieldTableLayoutPanel.Padding = new System.Windows.Forms.Padding(10);
            this.fieldTableLayoutPanel.RowCount = 6;
            this.fieldTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.fieldTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.fieldTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.fieldTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.fieldTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.fieldTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.fieldTableLayoutPanel.Size = new System.Drawing.Size(618, 297);
            this.fieldTableLayoutPanel.TabIndex = 4;
            // 
            // comboBox2
            // 
            this.comboBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(132, 156);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(473, 32);
            this.comboBox2.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(13, 153);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 38);
            this.label5.TabIndex = 8;
            this.label5.Text = "Chi tiết:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBox1
            // 
            this.comboBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(132, 118);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(473, 32);
            this.comboBox1.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(13, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 38);
            this.label4.TabIndex = 6;
            this.label4.Text = "Chức năng:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(13, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 35);
            this.label3.TabIndex = 4;
            this.label3.Text = "Người sửa:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox3
            // 
            this.textBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox3.Location = new System.Drawing.Point(132, 83);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(473, 29);
            this.textBox3.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(13, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 35);
            this.label2.TabIndex = 2;
            this.label2.Text = "Đến ngày:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox2
            // 
            this.textBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox2.Location = new System.Drawing.Point(132, 48);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(473, 29);
            this.textBox2.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(13, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 35);
            this.label1.TabIndex = 0;
            this.label1.Text = "Từ ngày:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(132, 13);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(473, 29);
            this.textBox1.TabIndex = 1;
            // 
            // overallTableLayoutPanel
            // 
            this.overallTableLayoutPanel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.overallTableLayoutPanel.ColumnCount = 1;
            this.overallTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.overallTableLayoutPanel.Controls.Add(this.panel3, 0, 1);
            this.overallTableLayoutPanel.Controls.Add(this.filterTableLayoutPanel, 0, 0);
            this.overallTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.overallTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.overallTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.overallTableLayoutPanel.Name = "overallTableLayoutPanel";
            this.overallTableLayoutPanel.RowCount = 2;
            this.overallTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.overallTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.overallTableLayoutPanel.Size = new System.Drawing.Size(1248, 607);
            this.overallTableLayoutPanel.TabIndex = 4;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.historyGroupBox);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 303);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1248, 304);
            this.panel3.TabIndex = 1;
            // 
            // historyGroupBox
            // 
            this.historyGroupBox.BackColor = System.Drawing.SystemColors.Control;
            this.historyGroupBox.Controls.Add(this.historyDataGridView);
            this.historyGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.historyGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.historyGroupBox.Location = new System.Drawing.Point(0, 0);
            this.historyGroupBox.Name = "historyGroupBox";
            this.historyGroupBox.Size = new System.Drawing.Size(1248, 304);
            this.historyGroupBox.TabIndex = 3;
            this.historyGroupBox.TabStop = false;
            this.historyGroupBox.Text = "Lịch Sử";
            // 
            // detailRichTextBox
            // 
            this.detailRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detailRichTextBox.Location = new System.Drawing.Point(3, 25);
            this.detailRichTextBox.Name = "detailRichTextBox";
            this.detailRichTextBox.Size = new System.Drawing.Size(612, 269);
            this.detailRichTextBox.TabIndex = 1;
            this.detailRichTextBox.Text = "";
            // 
            // historyDataGridView
            // 
            this.historyDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.historyDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.index,
            this.time,
            this.description,
            this.function,
            this.user});
            this.historyDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.historyDataGridView.Location = new System.Drawing.Point(3, 25);
            this.historyDataGridView.Name = "historyDataGridView";
            this.historyDataGridView.Size = new System.Drawing.Size(1242, 276);
            this.historyDataGridView.TabIndex = 1;
            // 
            // index
            // 
            this.index.HeaderText = "STT";
            this.index.Name = "index";
            // 
            // time
            // 
            this.time.HeaderText = "Thời gian";
            this.time.Name = "time";
            // 
            // description
            // 
            this.description.HeaderText = "Mô tả";
            this.description.Name = "description";
            this.description.Width = 240;
            // 
            // function
            // 
            this.function.HeaderText = "Chức năng";
            this.function.Name = "function";
            // 
            // user
            // 
            this.user.HeaderText = "Người đổi";
            this.user.Name = "user";
            // 
            // History
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.overallTableLayoutPanel);
            this.Name = "History";
            this.Size = new System.Drawing.Size(1248, 607);
            this.filterTableLayoutPanel.ResumeLayout(false);
            this.detailGroupBox.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.fieldTableLayoutPanel.ResumeLayout(false);
            this.fieldTableLayoutPanel.PerformLayout();
            this.overallTableLayoutPanel.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.historyGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.historyDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel filterTableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel overallTableLayoutPanel;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox detailGroupBox;
        private System.Windows.Forms.GroupBox historyGroupBox;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel fieldTableLayoutPanel;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.RichTextBox detailRichTextBox;
        private System.Windows.Forms.DataGridView historyDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn index;
        private System.Windows.Forms.DataGridViewTextBoxColumn time;
        private System.Windows.Forms.DataGridViewTextBoxColumn description;
        private System.Windows.Forms.DataGridViewTextBoxColumn function;
        private System.Windows.Forms.DataGridViewTextBoxColumn user;
    }
}
