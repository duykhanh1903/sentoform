﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Sento
{
    public partial class createInventory_UC : UserControl
    {
        private DBConnection SentoDB = DBConnection.Instance;
        public createInventory_UC()
        {
            InitializeComponent();
            FormLoad();
        }
        public void FormLoad()
        {
            DataGridViewSentoLoad();
            DataComboBoxDanhMuc();
            GrdSento.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }
        public void LoadDataGridSento()
        {
            GrdSento.DataSource = SentoDB.runScript("SELECT Sen.InventoryOfSentoID,Sen.InventoryOfSentoCode, " +
                "Sen.Name,Sen.CategoryID,Sen.ImagePath, Sen.Price,Sen.Discount,Sen.Unit,Sen.[Status] FROM dbo.InventoryOfSento AS Sen");
            foreach (DataGridViewColumn column in GrdSento.Columns)
            {
                if (column.HeaderText != "Check")
                {
                    column.ReadOnly = true;
                }
            }
        }
        public void DataGridViewSentoLoad()
        {
            GrdSento.DataSource = SentoDB.runScript("SELECT Sen.InventoryOfSentoID,Sen.InventoryOfSentoCode, " +
                "Sen.Name,Sen.CategoryID,Sen.ImagePath, Sen.Price,Sen.Discount,Sen.Unit,Sen.[Status] FROM dbo.InventoryOfSento AS Sen");
            foreach (DataGridViewColumn column in GrdSento.Columns)
            {
                if (column.HeaderText != "Check")
                {
                    column.ReadOnly = true;
                }
            }
            DataGridViewDisableButtonColumn EditColumn = new DataGridViewDisableButtonColumn();
            EditColumn.Name = "Edit";
            EditColumn.Text = "Update";
            EditColumn.HeaderText = "Chỉnh sửa";
            EditColumn.UseColumnTextForButtonValue = true;
            GrdSento.Columns.Add(EditColumn);
        }
        public void DataComboBoxDanhMuc()
        {
            string sql = "SELECT * FROM Category";
            CbDM.DataSource = SentoDB.runScript(sql);
            CbDM.DisplayMember = "Name";
            CbDM.ValueMember = "CategoryID";
        }
        private void btnCreate_Click(object sender, EventArgs e)
        {
            string status = "";
            if (CbStatus.Checked == true)
            {
                status = "Active";
            }
            else
                status = "Inactive";
            SentoDB.RunProcedure("usp_MappingCategoryToGetId");
            SqlParameter CategoryName = new SqlParameter("CategoryName", SqlDbType.NVarChar,100);
            CategoryName.Value = CbDM.Text;
            SqlParameter result = new SqlParameter("result", SqlDbType.Int);
            result.Direction = ParameterDirection.Output;
            result.Value = 0;
            SentoDB.AddParameters(CategoryName);
            SentoDB.AddParameters(result);
            SentoDB.LoadProcedure();
            int categoryid = (int)result.Value;

            //MessageBox.Show(cbDMdata.ToString());
            SentoDB.usp_InsertSentoProduct(txtMSP.Text,categoryid,txtName.Text, txtImagePath.Text, double.Parse(txtGV.Text),txtDV.Text,double.Parse(txtCK.Text), status);
            LoadDataGridSento();
            MessageBox.Show("Tạo sản phầm thành công");
        }

        private void btnImgSelect_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog()== System.Windows.Forms.DialogResult.OK)
            {
                string strfilename = openFileDialog1.FileName;
                txtImagePath.Text = strfilename;
            }
        }
        
        private void GrdSento_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            GrdSento.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            GrdSento.MultiSelect = false;
            DataGridViewRow row = GrdSento.Rows[e.RowIndex];
            string a = Converter.GetDetailString(row);

            List<string> ProductInfo = new List<string>();
            Dictionary<string, string> myDic = new Dictionary<string, string>
            {
                {"Mã sản phẩm",""},{"Tên sản phẩm",""},{"Danh mục",""},{"Hình ảnh",""},{"Giá vốn",""},{"Ngày cập nhật giá vốn",""},
                {"Giá bán","" },{"Ngày cập nhật giá bán",""},{"Chiết khấu",""},{"Đơn vị tính",""},{"Bảo hành",""},{"Trạng thái",""}
            };
            
            foreach (DataGridViewCell cell in row.Cells)
            {
                int colIndex = cell.ColumnIndex;
                string headerRowText = GrdSento.Columns[colIndex].HeaderText;
                foreach(KeyValuePair<string,string> item in myDic.ToList())
                {
                    if(item.Key == headerRowText)
                    {
                        if (cell.Value != null)
                        {
                            if(Int32.TryParse(cell.Value.ToString(),out _) == true)
                                myDic[item.Key] = Int32.Parse(cell.Value.ToString()).ToString();
                            myDic[item.Key] = cell.Value.ToString();
                        }
                    }
                }
            }
            rtbProductInfo.Clear();
            foreach(KeyValuePair<string, string> item in myDic.ToList())
            {
                if(item.Key == "Danh mục")
                {
                    SentoDB.RunProcedure("usp_GetNameWithIdCategory");
                    SqlParameter CategoryID = new SqlParameter("CategoryID", SqlDbType.Int);
                    CategoryID.Value = Int32.Parse(item.Value);
                    SqlParameter result = new SqlParameter("result", SqlDbType.NVarChar,100);
                    result.Direction = ParameterDirection.Output;
                    result.Value = string.Empty;
                    SentoDB.AddParameters(CategoryID);
                    SentoDB.AddParameters(result);
                    SentoDB.LoadProcedure();
                    myDic[item.Key] = result.Value.ToString();
                    rtbProductInfo.AppendText(item.Key + ": " + myDic[item.Key] + "\r\n");
                }
                else
                    rtbProductInfo.AppendText(item.Key + ": " + item.Value + "\r\n");
            }
            GrdSento.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect;
        }

        private void GrdSento_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCheckBoxCell checkBoxCell = (DataGridViewCheckBoxCell)GrdSento.Rows[e.RowIndex].Cells["Check"];
            try
            {
                if (checkBoxCell.Value.Equals(true) == true)
                {

                }
            }
            catch { }
        }


        private void GrdSento_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCell cell = GrdSento.CurrentCell;
            if (GrdSento.Columns[cell.ColumnIndex].HeaderText == "Check")
            {
                GrdSento.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
            var senderGrid = (DataGridView)sender;
            if(senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >=0)
            {
                DataGridViewDisableButtonCell buttonCell = (DataGridViewDisableButtonCell)GrdSento.Rows[e.RowIndex].Cells["Edit"];
                if(buttonCell.Enabled == true)
                {
                    try
                    {
                        SentoDB.OpenConnection();
                        DataGridViewRow row = GrdSento.CurrentRow;
                        SentoDB.RunProcedure("usp_UpdateInventoryOfSento");
                        SentoDB.cmd.Parameters.AddWithValue("@InventoryOfSentoID", Convert.ToInt32(row.Cells["InventoryOfSentoID"].Value));
                        SentoDB.cmd.Parameters.AddWithValue("@InventoryOfSentoCode", row.Cells["InventoryOfSentoCode"].Value == DBNull.Value ? null : row.Cells["InventoryOfSentoCode"].Value.ToString());
                        SentoDB.cmd.Parameters.AddWithValue("@Name", row.Cells["NameProduct"].Value == DBNull.Value ? null : row.Cells["NameProduct"].Value.ToString());
                        SentoDB.cmd.Parameters.AddWithValue("@CategoryID", row.Cells["CategoryID"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["CategoryID"].Value.ToString()));
                        SentoDB.cmd.Parameters.AddWithValue("@ImagePath", row.Cells["ImagePath"].Value == DBNull.Value ? null : row.Cells["ImagePath"].Value.ToString());
                        SentoDB.cmd.Parameters.AddWithValue("@Price", row.Cells["Price"].Value == DBNull.Value ? 0 : float.Parse(row.Cells["Price"].Value.ToString()));
                        SentoDB.cmd.Parameters.AddWithValue("@Unit", row.Cells["Unit"].Value == DBNull.Value ? null : row.Cells["Unit"].Value.ToString());
                        SentoDB.cmd.Parameters.AddWithValue("@Discount", row.Cells["Discount"].Value == DBNull.Value ? 0 : float.Parse(row.Cells["Discount"].Value.ToString()));
                        SentoDB.cmd.Parameters.AddWithValue("@Status", row.Cells["Status"].Value == DBNull.Value ? null : row.Cells["Status"].Value.ToString());
                        SentoDB.cmd.ExecuteNonQuery();
                        MessageBox.Show("Đã sửa thành công!!");
                        DataGridViewCheckBoxCell checkBoxCell = (DataGridViewCheckBoxCell)GrdSento.Rows[e.RowIndex].Cells["Check"];
                        if(checkBoxCell.Value.Equals(true)==true)
                        {
                            GrdSento.Rows[e.RowIndex].Cells["Check"].Value = false;
                        }
                        GrdSento.CommitEdit(DataGridViewDataErrorContexts.Commit);
                        SentoDB.CloseConnection();
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show(exception.Message);
                    }
                }
            }
        }

        private void GrdSento_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if(GrdSento.Columns[e.ColumnIndex].Name == "Check")
            {
                DataGridViewCheckBoxCell checkBoxCell = (DataGridViewCheckBoxCell)GrdSento.Rows[e.RowIndex].Cells["Check"];
                if (checkBoxCell.Value.Equals(true) == true)
                {
                    DataGridViewRow row = GrdSento.Rows[e.RowIndex];
                    foreach (DataGridViewCell cells in row.Cells)
                    {
                        string headerRowText = GrdSento.Columns[cells.ColumnIndex].HeaderText;
                        if (headerRowText != "Check")
                        {
                            cells.ReadOnly = false;
                        }
                    }
                    DataGridViewDisableButtonCell ButtonCell = (DataGridViewDisableButtonCell)GrdSento.Rows[e.RowIndex].Cells["Edit"];
                    ButtonCell.Enabled = (Boolean)checkBoxCell.Value;

                    GrdSento.Invalidate();
                }
                else
                {
                    DataGridViewRow row = GrdSento.Rows[e.RowIndex];
                    foreach (DataGridViewCell cells in row.Cells)
                    {
                        string headerRowText = GrdSento.Columns[cells.ColumnIndex].HeaderText;
                        if (headerRowText != "Check")
                        {
                            cells.ReadOnly = true;
                        }
                    }
                    DataGridViewDisableButtonCell ButtonCell = (DataGridViewDisableButtonCell)GrdSento.Rows[e.RowIndex].Cells["Edit"];
                    ButtonCell.Enabled = (Boolean)checkBoxCell.Value;

                    GrdSento.Invalidate();
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadDataGridSento();
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            if (txtNameToFind.Text != string.Empty)
            {
                DataTable dt = SentoDB.runScript("SELECT Sen.InventoryOfSentoID, Sen.InventoryOfSentoCode, Sen.Name,Sen.CategoryID,Sen.ImagePath," +
                        " Sen.Price,Sen.Discount,Sen.Unit,Sen.[Status] FROM InventoryOfSento AS Sen");
                DataView dv = dt.DefaultView;
                dv.RowFilter = string.Format("Name like '%{0}%'", txtNameToFind.Text);
                GrdSento.DataSource = dv.ToTable();
            }
            if (txtCodeToFind.Text != string.Empty)
            {
                DataTable dt = SentoDB.runScript("SELECT Sen.InventoryOfSentoID,Sen.InventoryOfSentoCode, Sen.Name,Sen.CategoryID,Sen.ImagePath," +
                        " Sen.Price,Sen.Discount,Sen.Unit,Sen.[Status] FROM InventoryOfSento AS Sen");
                DataView dv = dt.DefaultView;
                dv.RowFilter = string.Format("InventoryOfSentoCode like '%{0}%'", txtCodeToFind.Text);
                GrdSento.DataSource = dv.ToTable();
            }
        }

        private void txtNameToFind_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == (char)13)
            {
                //DataTable dt = SentoDB.runScript("SELECT Sen.InventoryOfSentoCode, Sen.Name,Sen.CategoryID,Sen.ImagePath," +
                //        " Sen.Price,Sen.Discount,Sen.Unit,Sen.[Status] FROM InventoryOfSento AS Sen");
                DataTable dt = SentoDB.runScript("SELECT Sen.InventoryOfSentoID, Sen.InventoryOfSentoCode, Sen.Name,Sen.CategoryID,Sen.ImagePath," +
                        " Sen.Price,Sen.Discount,Sen.Unit,Sen.[Status] FROM InventoryOfSento AS Sen");
                DataView dv = dt.DefaultView;
                dv.RowFilter = string.Format("Name like '%{0}%'", txtNameToFind.Text);
                GrdSento.DataSource = dv.ToTable();
            }
        }

        private void txtCodeToFind_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == (char)13)
            {
                DataTable dt = SentoDB.runScript("SELECT Sen.InventoryOfSentoID,Sen.InventoryOfSentoCode, Sen.Name,Sen.CategoryID,Sen.ImagePath," +
                        " Sen.Price,Sen.Discount,Sen.Unit,Sen.[Status] FROM InventoryOfSento AS Sen");
                DataView dv = dt.DefaultView;
                dv.RowFilter = string.Format("InventoryOfSentoCode like '%{0}%'", txtCodeToFind.Text);
                GrdSento.DataSource = dv.ToTable();
            }
        }
    }
    public class DataGridViewDisableButtonColumn : DataGridViewButtonColumn
    {
        public DataGridViewDisableButtonColumn()
        { 
            this.CellTemplate = new DataGridViewDisableButtonCell();
        }
    }

    public class DataGridViewDisableButtonCell : DataGridViewButtonCell
    {
        private bool enabledValue;
        public bool Enabled
        {
            get
            {
                return enabledValue;
            }
            set
            {
                enabledValue = value;
            }
        }

        // Override the Clone method so that the Enabled property is copied.
        public override object Clone()
        {
            DataGridViewDisableButtonCell cell =
                (DataGridViewDisableButtonCell)base.Clone();
            cell.Enabled = this.Enabled;
            return cell;
        }

        // By default, enable the button cell.
        public DataGridViewDisableButtonCell()
        {
            this.enabledValue = false;
        }

        protected override void Paint(Graphics graphics,
            Rectangle clipBounds, Rectangle cellBounds, int rowIndex,
            DataGridViewElementStates elementState, object value,
            object formattedValue, string errorText,
            DataGridViewCellStyle cellStyle,
            DataGridViewAdvancedBorderStyle advancedBorderStyle,
            DataGridViewPaintParts paintParts)
        {
            // The button cell is disabled, so paint the border,  
            // background, and disabled button for the cell.
            if (!this.enabledValue)
            {
                // Draw the cell background, if specified.
                if ((paintParts & DataGridViewPaintParts.Background) ==
                    DataGridViewPaintParts.Background)
                {
                    SolidBrush cellBackground =
                        new SolidBrush(cellStyle.BackColor);
                    graphics.FillRectangle(cellBackground, cellBounds);
                    cellBackground.Dispose();
                }

                // Draw the cell borders, if specified.
                if ((paintParts & DataGridViewPaintParts.Border) ==
                    DataGridViewPaintParts.Border)
                {
                    PaintBorder(graphics, clipBounds, cellBounds, cellStyle,
                        advancedBorderStyle);
                }

                // Calculate the area in which to draw the button.
                Rectangle buttonArea = cellBounds;
                Rectangle buttonAdjustment =
                    this.BorderWidths(advancedBorderStyle);
                buttonArea.X += buttonAdjustment.X;
                buttonArea.Y += buttonAdjustment.Y;
                buttonArea.Height -= buttonAdjustment.Height;
                buttonArea.Width -= buttonAdjustment.Width;

                // Draw the disabled button.                
                ButtonRenderer.DrawButton(graphics, buttonArea,
                    PushButtonState.Disabled);

                // Draw the disabled button text. 
                string text = "Update";
                try
                {
                    text = (string)this.FormattedValue;
                }
                catch
                {

                }
                TextRenderer.DrawText(graphics,
                    text,
                    this.DataGridView.Font,
                    buttonArea, SystemColors.GrayText);
            }
            else
            {
                // The button cell is enabled, so let the base class 
                // handle the painting.
                base.Paint(graphics, clipBounds, cellBounds, rowIndex,
                    elementState, value, formattedValue, errorText,
                    cellStyle, advancedBorderStyle, paintParts);
            }
        }
    }
}
