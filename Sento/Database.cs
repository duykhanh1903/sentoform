﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Sento
{
    class Database
    {
        private SqlConnection connect;
        private SqlDataAdapter adt;
        private SqlCommand cmd;

        private static Database singleton = null;
        public static Database Singleton
        {
            get
            {
                if (singleton == null)
                    singleton = new Database();
                return singleton;
            }
        }
        private Database()
        {
            string datasource = @"DESKTOP-UOEM5S0\SQLSEVER"; //Laptop
            //string datasource = @"DESKTOP-1U4F9V2\SQLEXPRESS"; //PC
            string database = "SentoDB";
            string connString = @"Data Source=" + datasource + ";Initial Catalog="
                        + database + ";Persist Security Info=True;Integrated Security=SSPI;";
            this.connect = new SqlConnection(connString);
        }
        public DataTable LoadData(string sqlquerry)
        {
            DataTable table = new DataTable();

            adt = new SqlDataAdapter(sqlquerry, this.connect);
            adt.Fill(table);
            return table;
        }
        public void SqlTransaction(string querry)
        {
            SqlTransaction transaction = connect.BeginTransaction();
            SqlCommand command = connect.CreateCommand();
            command.Transaction = transaction;
            try
            {
                command.CommandText = querry;
                command.ExecuteNonQuery();
                //new SqlCommand(querry, connect, transaction).ExecuteNonQuery();
                transaction.Commit();
            }
            catch(SqlException sqlError)
            {
                transaction.Rollback();
            }
            connect.Close();
        }
    }
}
