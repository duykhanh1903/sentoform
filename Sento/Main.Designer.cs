﻿namespace Sento
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.overallTabControl = new System.Windows.Forms.TabControl();
            this.manageTabPage = new System.Windows.Forms.TabPage();
            this.manageTabControl = new System.Windows.Forms.TabControl();
            this.createInventory_TPg = new System.Windows.Forms.TabPage();
            this.addInvetoryToSento_TPg = new System.Windows.Forms.TabPage();
            this.account_TPg = new System.Windows.Forms.TabPage();
            this.order_TPg = new System.Windows.Forms.TabPage();
            this.orderDetail_TPg = new System.Windows.Forms.TabControl();
            this.createOrder_TPg = new System.Windows.Forms.TabPage();
            this.approveOrder_TPg = new System.Windows.Forms.TabPage();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.receipt_TPg = new System.Windows.Forms.TabPage();
            this.receipt_TCtrl = new System.Windows.Forms.TabControl();
            this.createReceipt_TPg = new System.Windows.Forms.TabPage();
            this.approveReceipt_TPg = new System.Windows.Forms.TabPage();
            this.receiptDetail_TPg = new System.Windows.Forms.TabPage();
            this.permission_TPg = new System.Windows.Forms.TabPage();
            this.permission_TCtrl = new System.Windows.Forms.TabControl();
            this.permissionAccount_TPg = new System.Windows.Forms.TabPage();
            this.position_TPg = new System.Windows.Forms.TabPage();
            this.setting_TPg = new System.Windows.Forms.TabPage();
            this.setting_TCtrl = new System.Windows.Forms.TabControl();
            this.login_TPg = new System.Windows.Forms.TabPage();
            this.resetPassword_TPg = new System.Windows.Forms.TabPage();
            this.createInventory_UC1 = new Sento.createInventory_UC();
            this.addNewInventoryToSento_UC1 = new Sento.addNewInventoryToSento_UC();
            this.account_UC1 = new Sento.account_UC();
            this.createOrder_UC1 = new Sento.createOrder_UC();
            this.approveOrder_UC1 = new Sento.approveOrder_UC();
            this.s0031 = new Sento.orderDetail_UC();
            this.createReceipt_UC2 = new Sento.createReceipt_UC();
            this.approveReceipt_UC1 = new Sento.approveReceipt_UC();
            this.receiptDetail_UC1 = new Sento.receiptDetail_UC();
            this.permission_UC1 = new Sento.permission_UC();
            this.position_UC1 = new Sento.permissionGroup_UC();
            this.login_UC1 = new Sento.login_UC();
            this.resetPassword_UC1 = new Sento.resetPassword_UC();
            this.statusStrip1.SuspendLayout();
            this.overallTabControl.SuspendLayout();
            this.manageTabPage.SuspendLayout();
            this.manageTabControl.SuspendLayout();
            this.createInventory_TPg.SuspendLayout();
            this.addInvetoryToSento_TPg.SuspendLayout();
            this.account_TPg.SuspendLayout();
            this.order_TPg.SuspendLayout();
            this.orderDetail_TPg.SuspendLayout();
            this.createOrder_TPg.SuspendLayout();
            this.approveOrder_TPg.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.receipt_TPg.SuspendLayout();
            this.receipt_TCtrl.SuspendLayout();
            this.createReceipt_TPg.SuspendLayout();
            this.approveReceipt_TPg.SuspendLayout();
            this.receiptDetail_TPg.SuspendLayout();
            this.permission_TPg.SuspendLayout();
            this.permission_TCtrl.SuspendLayout();
            this.permissionAccount_TPg.SuspendLayout();
            this.position_TPg.SuspendLayout();
            this.setting_TPg.SuspendLayout();
            this.setting_TCtrl.SuspendLayout();
            this.login_TPg.SuspendLayout();
            this.resetPassword_TPg.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripDropDownButton1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 660);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1264, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(87, 17);
            this.toolStripStatusLabel1.Text = "Cài đặt hiển thị";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(29, 20);
            this.toolStripDropDownButton1.Text = "Cài đặt hiển thị";
            // 
            // overallTabControl
            // 
            this.overallTabControl.Controls.Add(this.manageTabPage);
            this.overallTabControl.Controls.Add(this.order_TPg);
            this.overallTabControl.Controls.Add(this.receipt_TPg);
            this.overallTabControl.Controls.Add(this.permission_TPg);
            this.overallTabControl.Controls.Add(this.setting_TPg);
            this.overallTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.overallTabControl.Location = new System.Drawing.Point(0, 0);
            this.overallTabControl.Margin = new System.Windows.Forms.Padding(0);
            this.overallTabControl.Name = "overallTabControl";
            this.overallTabControl.Padding = new System.Drawing.Point(0, 0);
            this.overallTabControl.SelectedIndex = 0;
            this.overallTabControl.Size = new System.Drawing.Size(1264, 660);
            this.overallTabControl.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.overallTabControl.TabIndex = 4;
            // 
            // manageTabPage
            // 
            this.manageTabPage.BackColor = System.Drawing.SystemColors.Control;
            this.manageTabPage.Controls.Add(this.manageTabControl);
            this.manageTabPage.Location = new System.Drawing.Point(4, 25);
            this.manageTabPage.Margin = new System.Windows.Forms.Padding(0);
            this.manageTabPage.Name = "manageTabPage";
            this.manageTabPage.Size = new System.Drawing.Size(1256, 631);
            this.manageTabPage.TabIndex = 0;
            this.manageTabPage.Text = "Quản lý";
            // 
            // manageTabControl
            // 
            this.manageTabControl.Controls.Add(this.createInventory_TPg);
            this.manageTabControl.Controls.Add(this.addInvetoryToSento_TPg);
            this.manageTabControl.Controls.Add(this.account_TPg);
            this.manageTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.manageTabControl.Location = new System.Drawing.Point(0, 0);
            this.manageTabControl.Margin = new System.Windows.Forms.Padding(0);
            this.manageTabControl.Name = "manageTabControl";
            this.manageTabControl.Padding = new System.Drawing.Point(0, 0);
            this.manageTabControl.SelectedIndex = 0;
            this.manageTabControl.Size = new System.Drawing.Size(1256, 631);
            this.manageTabControl.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.manageTabControl.TabIndex = 0;
            // 
            // createInventory_TPg
            // 
            this.createInventory_TPg.BackColor = System.Drawing.SystemColors.Control;
            this.createInventory_TPg.Controls.Add(this.createInventory_UC1);
            this.createInventory_TPg.Location = new System.Drawing.Point(4, 25);
            this.createInventory_TPg.Margin = new System.Windows.Forms.Padding(0);
            this.createInventory_TPg.Name = "createInventory_TPg";
            this.createInventory_TPg.Size = new System.Drawing.Size(1248, 602);
            this.createInventory_TPg.TabIndex = 2;
            this.createInventory_TPg.Text = "Tạo sản phẩm";
            // 
            // addInvetoryToSento_TPg
            // 
            this.addInvetoryToSento_TPg.BackColor = System.Drawing.SystemColors.Control;
            this.addInvetoryToSento_TPg.Controls.Add(this.addNewInventoryToSento_UC1);
            this.addInvetoryToSento_TPg.Location = new System.Drawing.Point(4, 25);
            this.addInvetoryToSento_TPg.Margin = new System.Windows.Forms.Padding(0);
            this.addInvetoryToSento_TPg.Name = "addInvetoryToSento_TPg";
            this.addInvetoryToSento_TPg.Size = new System.Drawing.Size(1248, 602);
            this.addInvetoryToSento_TPg.TabIndex = 0;
            this.addInvetoryToSento_TPg.Text = "Thêm sản phảm mới";
            // 
            // account_TPg
            // 
            this.account_TPg.BackColor = System.Drawing.SystemColors.Control;
            this.account_TPg.Controls.Add(this.account_UC1);
            this.account_TPg.Location = new System.Drawing.Point(4, 25);
            this.account_TPg.Margin = new System.Windows.Forms.Padding(0);
            this.account_TPg.Name = "account_TPg";
            this.account_TPg.Size = new System.Drawing.Size(1248, 602);
            this.account_TPg.TabIndex = 1;
            this.account_TPg.Text = "Tài khoản";
            // 
            // order_TPg
            // 
            this.order_TPg.BackColor = System.Drawing.SystemColors.Control;
            this.order_TPg.Controls.Add(this.orderDetail_TPg);
            this.order_TPg.Location = new System.Drawing.Point(4, 25);
            this.order_TPg.Margin = new System.Windows.Forms.Padding(0);
            this.order_TPg.Name = "order_TPg";
            this.order_TPg.Size = new System.Drawing.Size(1256, 631);
            this.order_TPg.TabIndex = 2;
            this.order_TPg.Text = "Đơn hàng";
            // 
            // orderDetail_TPg
            // 
            this.orderDetail_TPg.Controls.Add(this.createOrder_TPg);
            this.orderDetail_TPg.Controls.Add(this.approveOrder_TPg);
            this.orderDetail_TPg.Controls.Add(this.tabPage1);
            this.orderDetail_TPg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.orderDetail_TPg.Location = new System.Drawing.Point(0, 0);
            this.orderDetail_TPg.Margin = new System.Windows.Forms.Padding(0);
            this.orderDetail_TPg.Name = "orderDetail_TPg";
            this.orderDetail_TPg.Padding = new System.Drawing.Point(0, 0);
            this.orderDetail_TPg.SelectedIndex = 0;
            this.orderDetail_TPg.Size = new System.Drawing.Size(1256, 631);
            this.orderDetail_TPg.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.orderDetail_TPg.TabIndex = 2;
            // 
            // createOrder_TPg
            // 
            this.createOrder_TPg.BackColor = System.Drawing.SystemColors.Control;
            this.createOrder_TPg.Controls.Add(this.createOrder_UC1);
            this.createOrder_TPg.Location = new System.Drawing.Point(4, 25);
            this.createOrder_TPg.Margin = new System.Windows.Forms.Padding(0);
            this.createOrder_TPg.Name = "createOrder_TPg";
            this.createOrder_TPg.Size = new System.Drawing.Size(1248, 602);
            this.createOrder_TPg.TabIndex = 0;
            this.createOrder_TPg.Text = "Tạo đơn hàng";
            // 
            // approveOrder_TPg
            // 
            this.approveOrder_TPg.BackColor = System.Drawing.SystemColors.Control;
            this.approveOrder_TPg.Controls.Add(this.approveOrder_UC1);
            this.approveOrder_TPg.Location = new System.Drawing.Point(4, 25);
            this.approveOrder_TPg.Margin = new System.Windows.Forms.Padding(0);
            this.approveOrder_TPg.Name = "approveOrder_TPg";
            this.approveOrder_TPg.Size = new System.Drawing.Size(1248, 605);
            this.approveOrder_TPg.TabIndex = 1;
            this.approveOrder_TPg.Text = "Xét duyệt đơn hàng";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.s0031);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1248, 605);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "Chi tiết đơn hàng";
            // 
            // receipt_TPg
            // 
            this.receipt_TPg.BackColor = System.Drawing.SystemColors.Control;
            this.receipt_TPg.Controls.Add(this.receipt_TCtrl);
            this.receipt_TPg.Location = new System.Drawing.Point(4, 25);
            this.receipt_TPg.Margin = new System.Windows.Forms.Padding(0);
            this.receipt_TPg.Name = "receipt_TPg";
            this.receipt_TPg.Size = new System.Drawing.Size(1256, 631);
            this.receipt_TPg.TabIndex = 3;
            this.receipt_TPg.Text = "Phiếu nhập";
            // 
            // receipt_TCtrl
            // 
            this.receipt_TCtrl.Controls.Add(this.createReceipt_TPg);
            this.receipt_TCtrl.Controls.Add(this.approveReceipt_TPg);
            this.receipt_TCtrl.Controls.Add(this.receiptDetail_TPg);
            this.receipt_TCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.receipt_TCtrl.Location = new System.Drawing.Point(0, 0);
            this.receipt_TCtrl.Margin = new System.Windows.Forms.Padding(0);
            this.receipt_TCtrl.Name = "receipt_TCtrl";
            this.receipt_TCtrl.Padding = new System.Drawing.Point(0, 0);
            this.receipt_TCtrl.SelectedIndex = 0;
            this.receipt_TCtrl.Size = new System.Drawing.Size(1256, 631);
            this.receipt_TCtrl.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.receipt_TCtrl.TabIndex = 3;
            // 
            // createReceipt_TPg
            // 
            this.createReceipt_TPg.BackColor = System.Drawing.SystemColors.Control;
            this.createReceipt_TPg.Controls.Add(this.createReceipt_UC2);
            this.createReceipt_TPg.Location = new System.Drawing.Point(4, 25);
            this.createReceipt_TPg.Margin = new System.Windows.Forms.Padding(0);
            this.createReceipt_TPg.Name = "createReceipt_TPg";
            this.createReceipt_TPg.Size = new System.Drawing.Size(1248, 602);
            this.createReceipt_TPg.TabIndex = 0;
            this.createReceipt_TPg.Text = "Tạo phiếu nhập";
            // 
            // approveReceipt_TPg
            // 
            this.approveReceipt_TPg.BackColor = System.Drawing.SystemColors.Control;
            this.approveReceipt_TPg.Controls.Add(this.approveReceipt_UC1);
            this.approveReceipt_TPg.Location = new System.Drawing.Point(4, 25);
            this.approveReceipt_TPg.Margin = new System.Windows.Forms.Padding(0);
            this.approveReceipt_TPg.Name = "approveReceipt_TPg";
            this.approveReceipt_TPg.Size = new System.Drawing.Size(1248, 605);
            this.approveReceipt_TPg.TabIndex = 1;
            this.approveReceipt_TPg.Text = "Xét duyệt phiếu nhập";
            // 
            // receiptDetail_TPg
            // 
            this.receiptDetail_TPg.BackColor = System.Drawing.SystemColors.Control;
            this.receiptDetail_TPg.Controls.Add(this.receiptDetail_UC1);
            this.receiptDetail_TPg.Location = new System.Drawing.Point(4, 25);
            this.receiptDetail_TPg.Margin = new System.Windows.Forms.Padding(0);
            this.receiptDetail_TPg.Name = "receiptDetail_TPg";
            this.receiptDetail_TPg.Size = new System.Drawing.Size(1248, 605);
            this.receiptDetail_TPg.TabIndex = 3;
            this.receiptDetail_TPg.Text = "Chi tiết phiếu nhập";
            // 
            // permission_TPg
            // 
            this.permission_TPg.BackColor = System.Drawing.SystemColors.Control;
            this.permission_TPg.Controls.Add(this.permission_TCtrl);
            this.permission_TPg.Location = new System.Drawing.Point(4, 25);
            this.permission_TPg.Margin = new System.Windows.Forms.Padding(0);
            this.permission_TPg.Name = "permission_TPg";
            this.permission_TPg.Size = new System.Drawing.Size(1256, 631);
            this.permission_TPg.TabIndex = 4;
            this.permission_TPg.Text = "Phân quyền";
            // 
            // permission_TCtrl
            // 
            this.permission_TCtrl.Controls.Add(this.permissionAccount_TPg);
            this.permission_TCtrl.Controls.Add(this.position_TPg);
            this.permission_TCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.permission_TCtrl.Location = new System.Drawing.Point(0, 0);
            this.permission_TCtrl.Margin = new System.Windows.Forms.Padding(0);
            this.permission_TCtrl.Name = "permission_TCtrl";
            this.permission_TCtrl.Padding = new System.Drawing.Point(0, 0);
            this.permission_TCtrl.SelectedIndex = 0;
            this.permission_TCtrl.Size = new System.Drawing.Size(1256, 634);
            this.permission_TCtrl.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.permission_TCtrl.TabIndex = 5;
            // 
            // permissionAccount_TPg
            // 
            this.permissionAccount_TPg.BackColor = System.Drawing.SystemColors.Control;
            this.permissionAccount_TPg.Controls.Add(this.permission_UC1);
            this.permissionAccount_TPg.Location = new System.Drawing.Point(4, 25);
            this.permissionAccount_TPg.Margin = new System.Windows.Forms.Padding(0);
            this.permissionAccount_TPg.Name = "permissionAccount_TPg";
            this.permissionAccount_TPg.Size = new System.Drawing.Size(1248, 605);
            this.permissionAccount_TPg.TabIndex = 4;
            this.permissionAccount_TPg.Text = "Phân quyền tài khoản";
            // 
            // position_TPg
            // 
            this.position_TPg.BackColor = System.Drawing.SystemColors.Control;
            this.position_TPg.Controls.Add(this.position_UC1);
            this.position_TPg.Location = new System.Drawing.Point(4, 25);
            this.position_TPg.Margin = new System.Windows.Forms.Padding(0);
            this.position_TPg.Name = "position_TPg";
            this.position_TPg.Size = new System.Drawing.Size(1248, 605);
            this.position_TPg.TabIndex = 5;
            this.position_TPg.Text = "Chức vụ";
            // 
            // setting_TPg
            // 
            this.setting_TPg.BackColor = System.Drawing.SystemColors.Control;
            this.setting_TPg.Controls.Add(this.setting_TCtrl);
            this.setting_TPg.Location = new System.Drawing.Point(4, 25);
            this.setting_TPg.Margin = new System.Windows.Forms.Padding(0);
            this.setting_TPg.Name = "setting_TPg";
            this.setting_TPg.Size = new System.Drawing.Size(1256, 631);
            this.setting_TPg.TabIndex = 5;
            this.setting_TPg.Text = "Cài đặt";
            // 
            // setting_TCtrl
            // 
            this.setting_TCtrl.Controls.Add(this.login_TPg);
            this.setting_TCtrl.Controls.Add(this.resetPassword_TPg);
            this.setting_TCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.setting_TCtrl.Location = new System.Drawing.Point(0, 0);
            this.setting_TCtrl.Margin = new System.Windows.Forms.Padding(0);
            this.setting_TCtrl.Name = "setting_TCtrl";
            this.setting_TCtrl.Padding = new System.Drawing.Point(0, 0);
            this.setting_TCtrl.SelectedIndex = 0;
            this.setting_TCtrl.Size = new System.Drawing.Size(1256, 631);
            this.setting_TCtrl.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.setting_TCtrl.TabIndex = 3;
            // 
            // login_TPg
            // 
            this.login_TPg.BackColor = System.Drawing.SystemColors.Control;
            this.login_TPg.Controls.Add(this.login_UC1);
            this.login_TPg.Location = new System.Drawing.Point(4, 25);
            this.login_TPg.Margin = new System.Windows.Forms.Padding(0);
            this.login_TPg.Name = "login_TPg";
            this.login_TPg.Size = new System.Drawing.Size(1248, 602);
            this.login_TPg.TabIndex = 0;
            this.login_TPg.Text = "Đăng nhập";
            // 
            // resetPassword_TPg
            // 
            this.resetPassword_TPg.BackColor = System.Drawing.SystemColors.Control;
            this.resetPassword_TPg.Controls.Add(this.resetPassword_UC1);
            this.resetPassword_TPg.Location = new System.Drawing.Point(4, 25);
            this.resetPassword_TPg.Margin = new System.Windows.Forms.Padding(0);
            this.resetPassword_TPg.Name = "resetPassword_TPg";
            this.resetPassword_TPg.Size = new System.Drawing.Size(1248, 602);
            this.resetPassword_TPg.TabIndex = 1;
            this.resetPassword_TPg.Text = "Đặt lại mật khẩu";
            // 
            // createInventory_UC1
            // 
            this.createInventory_UC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.createInventory_UC1.Location = new System.Drawing.Point(0, 0);
            this.createInventory_UC1.Margin = new System.Windows.Forms.Padding(4);
            this.createInventory_UC1.Name = "createInventory_UC1";
            this.createInventory_UC1.Size = new System.Drawing.Size(1248, 602);
            this.createInventory_UC1.TabIndex = 0;
            // 
            // addNewInventoryToSento_UC1
            // 
            this.addNewInventoryToSento_UC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addNewInventoryToSento_UC1.Location = new System.Drawing.Point(0, 0);
            this.addNewInventoryToSento_UC1.Margin = new System.Windows.Forms.Padding(4);
            this.addNewInventoryToSento_UC1.Name = "addNewInventoryToSento_UC1";
            this.addNewInventoryToSento_UC1.Size = new System.Drawing.Size(1248, 605);
            this.addNewInventoryToSento_UC1.TabIndex = 0;
            // 
            // account_UC1
            // 
            this.account_UC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.account_UC1.Location = new System.Drawing.Point(0, 0);
            this.account_UC1.Margin = new System.Windows.Forms.Padding(5);
            this.account_UC1.Name = "account_UC1";
            this.account_UC1.Size = new System.Drawing.Size(1248, 605);
            this.account_UC1.TabIndex = 0;
            // 
            // createOrder_UC1
            // 
            this.createOrder_UC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.createOrder_UC1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createOrder_UC1.Location = new System.Drawing.Point(0, 0);
            this.createOrder_UC1.Margin = new System.Windows.Forms.Padding(4);
            this.createOrder_UC1.Name = "createOrder_UC1";
            this.createOrder_UC1.Size = new System.Drawing.Size(1248, 602);
            this.createOrder_UC1.TabIndex = 0;
            // 
            // approveOrder_UC1
            // 
            this.approveOrder_UC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.approveOrder_UC1.Location = new System.Drawing.Point(0, 0);
            this.approveOrder_UC1.Margin = new System.Windows.Forms.Padding(4);
            this.approveOrder_UC1.Name = "approveOrder_UC1";
            this.approveOrder_UC1.Size = new System.Drawing.Size(1248, 608);
            this.approveOrder_UC1.TabIndex = 0;
            // 
            // s0031
            // 
            this.s0031.Dock = System.Windows.Forms.DockStyle.Fill;
            this.s0031.Location = new System.Drawing.Point(0, 0);
            this.s0031.Margin = new System.Windows.Forms.Padding(4);
            this.s0031.Name = "s0031";
            this.s0031.Size = new System.Drawing.Size(1248, 608);
            this.s0031.TabIndex = 0;
            // 
            // createReceipt_UC2
            // 
            this.createReceipt_UC2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.createReceipt_UC2.Location = new System.Drawing.Point(0, 0);
            this.createReceipt_UC2.Margin = new System.Windows.Forms.Padding(4);
            this.createReceipt_UC2.Name = "createReceipt_UC2";
            this.createReceipt_UC2.Size = new System.Drawing.Size(1248, 602);
            this.createReceipt_UC2.TabIndex = 0;
            // 
            // approveReceipt_UC1
            // 
            this.approveReceipt_UC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.approveReceipt_UC1.Location = new System.Drawing.Point(0, 0);
            this.approveReceipt_UC1.Margin = new System.Windows.Forms.Padding(4);
            this.approveReceipt_UC1.Name = "approveReceipt_UC1";
            this.approveReceipt_UC1.Size = new System.Drawing.Size(1248, 608);
            this.approveReceipt_UC1.TabIndex = 0;
            // 
            // receiptDetail_UC1
            // 
            this.receiptDetail_UC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.receiptDetail_UC1.Location = new System.Drawing.Point(0, 0);
            this.receiptDetail_UC1.Margin = new System.Windows.Forms.Padding(4);
            this.receiptDetail_UC1.Name = "receiptDetail_UC1";
            this.receiptDetail_UC1.Size = new System.Drawing.Size(1248, 608);
            this.receiptDetail_UC1.TabIndex = 0;
            // 
            // permission_UC1
            // 
            this.permission_UC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.permission_UC1.Location = new System.Drawing.Point(0, 0);
            this.permission_UC1.Name = "permission_UC1";
            this.permission_UC1.Size = new System.Drawing.Size(1248, 605);
            this.permission_UC1.TabIndex = 0;
            // 
            // position_UC1
            // 
            this.position_UC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.position_UC1.Location = new System.Drawing.Point(0, 0);
            this.position_UC1.Margin = new System.Windows.Forms.Padding(5);
            this.position_UC1.Name = "position_UC1";
            this.position_UC1.Size = new System.Drawing.Size(1248, 608);
            this.position_UC1.TabIndex = 1;
            // 
            // login_UC1
            // 
            this.login_UC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.login_UC1.Location = new System.Drawing.Point(0, 0);
            this.login_UC1.Name = "login_UC1";
            this.login_UC1.Size = new System.Drawing.Size(1248, 602);
            this.login_UC1.TabIndex = 0;
            // 
            // resetPassword_UC1
            // 
            this.resetPassword_UC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resetPassword_UC1.Location = new System.Drawing.Point(0, 0);
            this.resetPassword_UC1.Margin = new System.Windows.Forms.Padding(4);
            this.resetPassword_UC1.Name = "resetPassword_UC1";
            this.resetPassword_UC1.Size = new System.Drawing.Size(1248, 602);
            this.resetPassword_UC1.TabIndex = 0;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 682);
            this.Controls.Add(this.overallTabControl);
            this.Controls.Add(this.statusStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sento";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.overallTabControl.ResumeLayout(false);
            this.manageTabPage.ResumeLayout(false);
            this.manageTabControl.ResumeLayout(false);
            this.createInventory_TPg.ResumeLayout(false);
            this.addInvetoryToSento_TPg.ResumeLayout(false);
            this.account_TPg.ResumeLayout(false);
            this.order_TPg.ResumeLayout(false);
            this.orderDetail_TPg.ResumeLayout(false);
            this.createOrder_TPg.ResumeLayout(false);
            this.approveOrder_TPg.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.receipt_TPg.ResumeLayout(false);
            this.receipt_TCtrl.ResumeLayout(false);
            this.createReceipt_TPg.ResumeLayout(false);
            this.approveReceipt_TPg.ResumeLayout(false);
            this.receiptDetail_TPg.ResumeLayout(false);
            this.permission_TPg.ResumeLayout(false);
            this.permission_TCtrl.ResumeLayout(false);
            this.permissionAccount_TPg.ResumeLayout(false);
            this.position_TPg.ResumeLayout(false);
            this.setting_TPg.ResumeLayout(false);
            this.setting_TCtrl.ResumeLayout(false);
            this.login_TPg.ResumeLayout(false);
            this.resetPassword_TPg.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.TabControl overallTabControl;
        private System.Windows.Forms.TabPage manageTabPage;
        private System.Windows.Forms.TabPage order_TPg;
        private System.Windows.Forms.TabControl orderDetail_TPg;
        private System.Windows.Forms.TabPage createOrder_TPg;
        private System.Windows.Forms.TabPage approveOrder_TPg;
        private System.Windows.Forms.TabPage tabPage1;
        private approveOrder_UC approveOrder_UC1;
        private orderDetail_UC s0031;
        private System.Windows.Forms.TabPage receipt_TPg;
        private System.Windows.Forms.TabControl receipt_TCtrl;
        private System.Windows.Forms.TabPage createReceipt_TPg;
        private System.Windows.Forms.TabPage approveReceipt_TPg;
        private System.Windows.Forms.TabPage receiptDetail_TPg;
        private createOrder_UC createOrder_UC1;
        private createReceipt_UC createReceipt_UC2;
        private approveReceipt_UC approveReceipt_UC1;
        private receiptDetail_UC receiptDetail_UC1;
        private System.Windows.Forms.TabControl manageTabControl;
        private System.Windows.Forms.TabPage addInvetoryToSento_TPg;
        private System.Windows.Forms.TabPage account_TPg;
        private addNewInventoryToSento_UC addNewInventoryToSento_UC1;
        private System.Windows.Forms.TabPage permission_TPg;
        private System.Windows.Forms.TabControl permission_TCtrl;
        private System.Windows.Forms.TabPage permissionAccount_TPg;
        private permission_UC permission_UC1;
        private System.Windows.Forms.TabPage position_TPg;
        private permissionGroup_UC position_UC1;
        private System.Windows.Forms.TabPage setting_TPg;
        private System.Windows.Forms.TabControl setting_TCtrl;
        private System.Windows.Forms.TabPage login_TPg;
        private System.Windows.Forms.TabPage resetPassword_TPg;
        private login_UC login_UC1;
        private resetPassword_UC resetPassword_UC1;
        private account_UC account_UC1;
        private System.Windows.Forms.TabPage createInventory_TPg;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private createInventory_UC createInventory_UC1;
    }
}

